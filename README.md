#Docker Files for Various Projects

The goal of this repository is to provide docker images that host a complete environement to promote and quickly show the capability of Butor framework.
~~~~
##Currently Available

* java creates a basic Ubuntu 14.04 image with Oracle JDK 7
* mule is based on codingtony/java and installs a vanilla version of Mule ESB 3.5.0
* tomcat7 is based on codingtony/tomcat7 and installs a vanilla version of Tomcat 7.0.54

##Planned

* haproxy : Haproxy 1.5
* mysql : MySQL (or MariaDB) 5.5
* butor-demo webapp
* butor-demo services
* butor-demo data
